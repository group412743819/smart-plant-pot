# Smart Plant Pot

This repository contains the code and instructions for setting up a smart plant pot system. The system includes four main components:

1. **Node-RED**
2. **ThingsBoard**
3. **Web-App**
4. **Python Script**

## Prerequisites

Before you begin, ensure you have the following:

- A Raspberry Pi device
- A camera module
- A temperature and humidity sensor
- An LCD screen
- ThingsBoard account
- Node-RED installed on your Raspberry Pi
- Python installed on your Raspberry Pi

## Setup Instructions

### 1. Setting up the Raspberry Pi

1. Connect the camera module, temperature and humidity sensor, and LCD screen to your Raspberry Pi.
2. Power on your Raspberry Pi and connect it to the internet.

### 2. Configuring ThingsBoard

1. Log in to your ThingsBoard account.
2. Follow the IoT lab book instructions to set up your Raspberry Pi device in ThingsBoard.
3. Import the provided JSON files for the rule chains:
   - Go to the Rule Chains section in ThingsBoard.
   - Click on the import button and select the JSON file provided in this repository.

### 3. Configuring Node-RED

1. Open Node-RED on your Raspberry Pi.
2. Import the provided JSON files for the flows:
   - Go to the Node-RED interface.
   - Click on the menu, then Import, and select the JSON file provided in this repository.
3. Set up the daemon to run the Python script:
   - In Node-RED, configure the appropriate node to execute `smart_plant_script.py` as required.

### 4. Running the Web-App

1. Ensure you have all necessary Python packages installed:
   -flask
2. Run the Web App locally or using a webserver.

