import time

import sys

import os

import grovepi

import math

import json

from picamera import PiCamera

from datetime import datetime, timedelta

import schedule


# Sensor and device configuration

sensor = 4  # DHT sensor on digital port 4

blue = 0    # The Blue colored sensor

buzzer = 8  # Buzzer on digital port 8


# Check the platform and input the appropriate smbus library

if sys.platform == 'uwp':

    import winrt_smbus as smbus

    bus = smbus.SMBus(1)

else:

    import smbus

    import RPi.GPIO as GPIO

    bus = smbus.SMBus(1 if GPIO.RPI_REVISION in [2, 3] else 0)

 
# Define the addresses for the display

DISPLAY_RGB_ADDR = 0x30

DISPLAY_TEXT_ADDR = 0x3e

 

# Function to set the RGB values of the display

def setRGB(r, g, b):

    bus.write_byte_data(DISPLAY_RGB_ADDR, 0, 0)

    bus.write_byte_data(DISPLAY_RGB_ADDR, 1, 0)

    bus.write_byte_data(DISPLAY_RGB_ADDR, 0x08, 0xaa)

    bus.write_byte_data(DISPLAY_RGB_ADDR, 4, r)

    bus.write_byte_data(DISPLAY_RGB_ADDR, 3, g)

    bus.write_byte_data(DISPLAY_RGB_ADDR, 2, b)

 
 # Function to send a command to the text display

def textCommand(cmd):

    bus.write_byte_data(DISPLAY_TEXT_ADDR, 0x80, cmd)

 
# Function to set the text of the display

def setText(text):

    textCommand(0x01)  # Clear display

    time.sleep(0.05)

    textCommand(0x08 | 0x04)  # Display on, no cursor

    textCommand(0x28)  # 2 lines

    time.sleep(0.05)

    count = 0

    row = 0

    for c in text:

        if c == '\n' or count == 16:

            count = 0

            row += 1

            if row == 2:

                break

            textCommand(0xc0)

            if c == '\n':

                continue

        count += 1

        bus.write_byte_data(DISPLAY_TEXT_ADDR, 0x40, ord(c))

 
# Function to take a picture with the PiCamera

def take_plant_picture():

    try:

        camera = PiCamera()  # Initialize the PiCamera

        camera.resolution = (1024, 768)  # Set camera resolution

        camera.start_preview()  # Start camera preview

        time.sleep(2)   # Allow time for camera to adjust

        timestamp = datetime.now().strftime("%Y%m%d_%H%M%S")    # Generate timestamp for filename

        filename = f"/home/pi/Pictures/plant_{timestamp}.jpg"   # Set filename with timestamp

        camera.capture(filename)    # Capture and save image

        camera.stop_preview()   # Stop camera preview

        camera.close()  # Close camera

        print(f"Picture taken and saved as {filename}") # Print success message

    except Exception as e:

        print(f"An error occurred: {e}")    # Print error message if something goes wrong

 
# Function to schedule the picture taking

def schedule_picture():

    formatted_time = "16:31"    # Set the time for taking pictures

    schedule.every().day.at(formatted_time).do(take_plant_picture)  # Schedule the picture taking

    print(f"Scheduled to take a picture at {formatted_time}")   # Print confirmation message


# Function to update display based on humidity and temperature checks 

def check_conditions_and_update_display(temp, humidity):

    status_text = ""
    if humidity > 70:
        
        time.sleep(0.1)
        
        setRGB(250, 5, 100) # Set display color to blue
        
        status_text = f"Danger! High \nHumidity: {humidity}%"    # Set status text
        grovepi.digitalWrite(buzzer, 1)  # Activate buzzer

        time.sleep(0.3)

        grovepi.digitalWrite(buzzer, 0) # Deactivate buzzer
        
    elif humidity < 25:
        
        setRGB(250, 5, 100) # Set display color to blue
        
        status_text = f"Danger! Low \nHumidity: {humidity}%"    # Set status text
        grovepi.digitalWrite(buzzer, 1)  # Activate buzzer

        time.sleep(0.3)

        grovepi.digitalWrite(buzzer, 0) # Deactivate buzzer
        
    elif temp > 28:

        setRGB(250, 5, 100) # Set display color to blue

        status_text = f"Danger! High \nTemp: {temp}C"   # Set status text
        grovepi.digitalWrite(buzzer, 1) # Activate buzzer

        time.sleep(0.3)

        grovepi.digitalWrite(buzzer, 0) # Deactivate buzzer


    elif temp < 15:

        setRGB(250, 5, 100) # Set display color to blue

        status_text = f"Danger! Low \nTemp: {temp}C"    # Set status text
        grovepi.digitalWrite(buzzer, 1)  # Activate buzzer

        time.sleep(0.3)

        grovepi.digitalWrite(buzzer, 0) # Deactivate buzzer

    else:

        setRGB(5, 250, 0)   # Set display color to green

        status_text = f"Temp: {temp}C, \nHumidity: {humidity}%" # Set status text for normal conditions

    setText(status_text)    # Update the display with status text

if __name__ == "__main__":

    schedule_picture()  # Schedule the picture taking

    setRGB(5, 250, 0)   # Initialize display color to green

    while True:

        try:

            schedule.run_pending()  # Run scheduled tasks

            time.sleep(1)   # Wait for a second

            [temp, humidity] = grovepi.dht(sensor, blue)    # Read temperature and humidity from sensor

            if not math.isnan(temp) and not math.isnan(humidity):   # Check if readings are valid

                check_conditions_and_update_display(temp, humidity) # Update display based on readings

                data_json = json.dumps({'temp': temp, 'humidity': humidity})    # Convert readings to JSON for telemetry data

                time.sleep(0.1)

                sys.stdout.write(data_json + '\n')  # Print JSON data

        except KeyboardInterrupt:

            print("Terminated.")    # Print termination message

            os._exit(0) # Exit program

        except Exception as e:

            print(f"Unhandled error: {e}")  # Print any unhandled error messages