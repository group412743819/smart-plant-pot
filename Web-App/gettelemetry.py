import requests
import time
import json

# Replace with your ThingsBoard URL and device access token
url = "https://thingsboard.cs.cf.ac.uk/api/plugins/telemetry/DEVICE/1708f690-fe56-11ee-b56b-a96a8be1c6f5/values/timeseries"

while True:

    # Create data payload
    headers = {
        'Content-Type': 'application/json',
        'X-Authorization': 'Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJncm91cDQxQGNhcmRpZmYuYWMudWsiLCJzY29wZXMiOlsiVEVOQU5UX0FETUlOIl0sInVzZXJJZCI6IjllYjIwYWEwLTlmZmItMTFlZS05M2M4LTliMWNmZDRkMDYyZiIsImVuYWJsZWQiOnRydWUsImlzUHVibGljIjpmYWxzZSwidGVuYW50SWQiOiI5YTAzMDE4MC05ZmZiLTExZWUtOTNjOC05YjFjZmQ0ZDA2MmYiLCJjdXN0b21lcklkIjoiMTM4MTQwMDAtMWRkMi0xMWIyLTgwODAtODA4MDgwODA4MDgwIiwiaXNzIjoidGhpbmdzYm9hcmQuaW8iLCJpYXQiOjE3MTUwMDU4MzAsImV4cCI6MTcxNTAxNDgzMH0.VuGhyswz9QbsQtaBgP9RSYxEPKiJoUU1A0y6gjxDovhg6mp-IsOYWqcGe3ndEWLtjlKgVpbnQlo1RJeQ0Dpd6w',
    }

    # Send data to ThingsBoard
    response = requests.get(url, headers=headers)

    try:
        data = response.json()
    except json.JSONDecodeError:
        print("invalid")
    
    print(data["humidity"][0]["value"])
    # Wait for 1 second
    time.sleep(1)