# Import necessary modules from Flask
from flask import Flask, render_template, jsonify
# Import the requests module to handle HTTP requests
import requests

# Initialize the Flask application
app = Flask(__name__)

# Define a route for the root URL and "/fetch" URL
@app.route("/")
@app.route("/fetch")
def fetch():
    # Infinite loop (note: typically you wouldn't use an infinite loop in a web route)
    while True:
        # URL to fetch telemetry data from ThingsBoard API
        url = "https://thingsboard.cs.cf.ac.uk/api/plugins/telemetry/DEVICE/1708f690-fe56-11ee-b56b-a96a8be1c6f5/values/timeseries"
        # URL to get the authentication token
        urltoken = "https://thingsboard.cs.cf.ac.uk/api/auth/login"
        # Credentials for authentication
        credentials = {"username": "group41@cardiff.ac.uk", "password": "group412024"}

        # Send a POST request to get the authentication token
        responsetoken = requests.post(urltoken, json=credentials)
        # Extract the token from the response
        token = responsetoken.json()["token"]

        # Set up the headers for the GET request, including the authorization token
        headers = {
            'Content-Type': 'application/json',
            'X-Authorization': 'Bearer ' + token,
        }
        # Send a GET request to fetch the telemetry data
        response = requests.get(url, headers=headers)
        # Parse the response as JSON
        data = response.json()
        # Render the 'home.html' template with the fetched data
        return render_template('home.html', data=data)

# Run the application if this script is executed directly
if __name__ == "__main__":
    app.run(debug=True)

