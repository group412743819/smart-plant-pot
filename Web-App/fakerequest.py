import requests
import random
import time

# Replace with your ThingsBoard URL and device access token
url = "https://thingsboard.cs.cf.ac.uk/api/v1/group41/telemetry"

while True:
    # Generate fake temperature
    temperature = random.uniform(14,27)
    humidity = random.uniform(26,69)

    # Create data payload
    data = {"temperature": temperature, "humidity": humidity}

    try:
        # Send data to ThingsBoard
        requests.post(url, json=data)
        print("sent")
    except requests.exceptions.RequestException as e:
        print("An error occurred:", e)

    # Wait for 1 second
    time.sleep(1)
